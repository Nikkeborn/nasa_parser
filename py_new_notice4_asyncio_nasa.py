import aiohttp
import asyncio
import json

# APOD_QUERY = 'https://api.nasa.gov/planetary/apod?api_key=RFOgR1BEX2rVVH9H0CoR0AJQd4crkdzES9CzW3Yd'
# EPIC_QUERY = 'https://api.nasa.gov/EPIC/api/natural/images?api_key=RFOgR1BEX2rVVH9H0CoR0AJQd4crkdzES9CzW3Yd'
APOD = 'https://api.nasa.gov/planetary/apod'
EPIC = 'https://api.nasa.gov/EPIC/api/natural/images'
QP = {'api_key' : 'RFOgR1BEX2rVVH9H0CoR0AJQd4crkdzES9CzW3Yd'}

async def FetchAPOD():
    async with aiohttp.ClientSession() as session:
        async with session.get(APOD, params=QP) as response:    
            print(response.url)
            print(response.status)
            # print(await response.text())
            # print(await response.json())
            return await response.json()


async def FileAPOD():
    with open('/home/wis/Desktop/apod.json', 'w') as apjs:
        json.dump(await FetchAPOD(), apjs, indent='	')
            

async def FetchEPIC():
    async with aiohttp.ClientSession() as session:
        async with session.get(EPIC, params=QP) as response:    
            print(response.url)
            print(response.status)
            # print(await response.text())
            # print(await response.json())
            return await response.json()


async def FileEPIC():
    with open('/home/wis/Desktop/EPIC.json', 'w') as epjs:
        json.dump(await FetchEPIC(), epjs, indent='	')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    tasks = [
        # loop.create_task(FetchAPOD()), 
        # loop.create_task(FetchEPIC()), 
        loop.create_task(FileAPOD()), 
        loop.create_task(FileEPIC()),
        ]
    wait_tasks = asyncio.wait(tasks)
    loop.run_until_complete(wait_tasks)
   